fn move_head(head: &mut (i64, i64), dir: &str) {
    match dir {
        "U" => head.1 -= 1,
        "D" => head.1 += 1,
        "R" => head.0 += 1,
        "L" => head.0 -= 1,
        _ => (),
    }
}

fn move_tail(head: &(i64, i64), tail: &mut (i64, i64)) {
    if head.0 > tail.0 + 1 && head.1 > tail.1 {
        tail.0 += 1;
        tail.1 += 1;
    } else if head.0 < tail.0 - 1 && head.1 < tail.1 {
        tail.0 -= 1;
        tail.1 -= 1;
    } else if head.0 > tail.0 + 1 && head.1 < tail.1 {
        tail.0 += 1;
        tail.1 -= 1;
    } else if head.0 < tail.0 - 1 && head.1 > tail.1 {
        tail.0 -= 1;
        tail.1 += 1;
    } else if head.0 > tail.0 && head.1 > tail.1 + 1 {
        tail.0 += 1;
        tail.1 += 1;
    } else if head.0 < tail.0 && head.1 < tail.1 - 1 {
        tail.0 -= 1;
        tail.1 -= 1;
    } else if head.0 > tail.0 && head.1 < tail.1 - 1 {
        tail.0 += 1;
        tail.1 -= 1;
    } else if head.0 < tail.0 && head.1 > tail.1 + 1 {
        tail.0 -= 1;
        tail.1 += 1;
    } else if head.0 > tail.0 + 1 {
        tail.0 += 1;
    } else if head.0 < tail.0 - 1 {
        tail.0 -= 1;
    } else if head.1 > tail.1 + 1 {
        tail.1 += 1;
    } else if head.1 < tail.1 - 1 {
        tail.1 -= 1;
    }
}

fn main() {
    let mut knots = vec![];
    for _ in 0..10 {
        knots.push((0i64, 0i64));
    }
    let mut tail_history = ::std::collections::BTreeSet::<(i64, i64)>::new();
    tail_history.insert((0i64, 0i64));
    for line in ::std::io::stdin().lines() {
        let Ok(line) = line else { continue };
        let mut iter = line.split_whitespace();
        let Some(dir) = iter.next() else { continue };
        let Some(count) = iter.next() else { continue };
        let Ok(count) = count.parse::<usize>() else { continue };
        for _ in 0..count {
            let head = knots.iter_mut().next().unwrap();
            move_head(head, dir);
            for i in 1..10 {
                let head = knots.iter().cloned().nth(i - 1).unwrap();
                let tail = knots.iter_mut().nth(i).unwrap();
                move_tail(&head, tail);
            }
            tail_history.insert(knots.iter().cloned().last().unwrap());
        }
    }
    println!("{}", tail_history.len());
}
